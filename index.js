import { NativeModules } from 'react-native';

const { PersianDatePickerDialog } = NativeModules;

export default PersianDatePickerDialog;
